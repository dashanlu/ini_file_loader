import scala.collection.mutable
import scala.io.Source

object ConfigLoaderApp extends App {
  override def main(args: Array[String]) = {
    if(args==null || args.length<1)
      print("please provide the full path to the config file")
    else {
      val iterator = Source.fromFile(args(0)).getLines()

      val defaultSettingNames = if(args.length>1)args(1).split(",").map(_.trim).toSet else Set.empty[String]
      val loaderComp = new ConfigRegexLoaderComp with ConfigRegexParserComp
      val configs = loaderComp.configLoader.load(iterator.toStream, None
        , new mutable.HashMap[String, Set[String]], new mutable.HashMap[String, mutable.Map[String, Set[String]]], defaultSettingNames)


      if (configs.isDefined) {
        println(configs.get)
      } else {
        println("something wrong with .ini file")
      }
    }
  }
}
