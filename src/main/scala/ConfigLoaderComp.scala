import scala.collection.mutable

trait ConfigLoaderComp {
  this: ConfigParserComp =>

  def configLoader: ConfigLoader

  trait ConfigLoader {
    def load(lines: Stream[String], curGroup: Option[String], curConfig: mutable.Map[String, Set[String]]
             , configs: mutable.Map[String, mutable.Map[String, Set[String]]]
             , defaultSet: Set[String] = Set.empty): Option[mutable.Map[String, mutable.Map[String, Set[String]]]]
  }

}
