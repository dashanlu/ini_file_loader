trait ConfigParserComp {
  def configParser:ConfigParser

  trait ConfigParser{
    def parse(line: String): Option[Item]
  }
}
