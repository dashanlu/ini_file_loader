import scala.collection.mutable

class ConfigRegexLoaderComp extends ConfigLoaderComp {
  this: ConfigParserComp =>
  def configLoader = new ConfigRegexLoader

  class ConfigRegexLoader extends ConfigLoader {
    def load(lines: Stream[String], curGroup: Option[String], curConfig: mutable.Map[String, Set[String]]
             , configs: mutable.Map[String, mutable.Map[String, Set[String]]]
             , defaultSet: Set[String] = Set.empty): Option[mutable.Map[String, mutable.Map[String, Set[String]]]] = {
      lines match {
        case x #:: xs => {
          configParser.parse(x) match {
            case Some(Group(name)) =>
              load(xs, Some(name), new mutable.HashMap[String, Set[String]]()
                , if (curGroup.isDefined) configs.updated(curGroup.get, curConfig) else configs, defaultSet)
            case Some(Setting(key, value)) => load(xs, curGroup, curConfig.updated(key, value), configs, defaultSet)
            case Some(DefaultSetting(key, default, value)) => {
              if (defaultSet.contains(default)) load(xs, curGroup, curConfig.updated(key, value), configs, defaultSet)
              else load(xs, curGroup, curConfig, configs, defaultSet)
            }
            case _ => None
          }

        }
        case _ => curGroup match {
          case None => None
          case _ => Some(configs.updated(curGroup.get, curConfig))
        }

      }
    }
  }

}
