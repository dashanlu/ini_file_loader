import java.util.regex.Pattern

trait ConfigRegexParserComp extends ConfigParserComp {
  def configParser = new ConfigRegexParser()

  class ConfigRegexParser extends ConfigParser {
    private val groupNameRegex = Pattern.compile("\\s*\\[\\s*(\\S+.+\\S+)\\s*\\]\\s*")
    private val regularSetting = Pattern.compile("\\s*([^=\\s<>;]+)\\s*=\\s*([^=^=;]+).*(;.*)?")
    private val defaultSetting = Pattern.compile("\\s*([^=\\s<>;]+)\\s*<([^;\\s<>]+)>\\s*=\\s*([^=^=;]+).*(;.*)?")

    def parse(line: String): Option[Item] = {
      val group = parseGroup(line)
      val setting = parseRegularSetting(line)
      val defaultSetting = parseSettingWithDefault(line)

      if (group.isDefined) {
        group
      } else if (setting.isDefined) {
        setting
      } else if (defaultSetting.isDefined) {
        defaultSetting
      } else {
        None
      }
    }

    private def parseGroup(line: String): Option[Item] = {
      val matcher = groupNameRegex.matcher(line)
      if (matcher.find()) {
        Some(Group(matcher.group(1)))
      } else {
        None
      }
    }

    private def parseRegularSetting(line: String): Option[Item] = {
      val matcher = regularSetting.matcher(line)
      if (matcher.find()) {
        Some(Setting(matcher.group(1), matcher.group(2).split(",").map(_.trim()).toSet))
      } else {
        None
      }
    }

    private def parseSettingWithDefault(line: String): Option[Item] = {
      val matcher = defaultSetting.matcher(line)
      if (matcher.find()) {
        Some(DefaultSetting(matcher.group(1), matcher.group(2), matcher.group(3).split(",").map(_.trim()).toSet))
      } else {
        None
      }
    }
  }

}
