
sealed abstract class Item

case class Group(name: String) extends Item

case class Setting(name: String, values: Set[String]) extends Item

case class DefaultSetting(name: String, default: String, values: Set[String]) extends Item
