trait RequestParserComp {
  def requestParser: RequestParser

  trait RequestParser {
    def parse(input: String): List[String]
  }

}
