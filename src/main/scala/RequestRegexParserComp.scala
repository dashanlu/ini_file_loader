import java.util.regex.Pattern

class RequestRegexParserComp extends RequestParserComp {
  override def requestParser: RequestParser = new RequestRegex

  class RequestRegex extends RequestParser {
    private val nestedPattern = Pattern.compile("(\\S+).*\\[(\\S+)\\]")
    private val regularPattern = Pattern.compile("[^\\s\\[\\]]+")

    override def parse(input: String): List[String] = {
      if (input.isEmpty) List.empty
      else {
        val levels = input.split("\\.").map(_.trim()).toList

        //        for {
        //          level <- levels
        //          result <- parseNode(level)
        //        } yield result
        levels flatMap { x => parseNode(x) }
      }

    }

    private def parseNode(node: String): List[String] = {
      val nestedMatcher = nestedPattern.matcher(node)
      if (nestedMatcher.find()) {
        List(nestedMatcher.group(1), nestedMatcher.group(2))
      } else {
        val regularMatcher = regularPattern.matcher(node)
        if (regularMatcher.find()) {
          List(regularMatcher.group(0))
        } else {
          List.empty
        }
      }
    }

  }

}
