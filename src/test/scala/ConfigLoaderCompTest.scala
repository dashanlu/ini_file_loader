import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable

class ConfigLoaderCompTest extends FlatSpec with Matchers {
  val configLoaderComp = new ConfigRegexLoaderComp with ConfigRegexParserComp

  "Load stream of simple Strings " should "create config" in {
    val lineStream = Stream("[test]", "key=value", "key1=value1")
    val configs = configLoaderComp.configLoader.load(lineStream, None
      , new mutable.HashMap[String, Set[String]], new mutable.HashMap[String, mutable.Map[String, Set[String]]])

    configs.isDefined shouldBe true
    val config = configs.get.head
    config._1 shouldBe "test"
    config._2.isEmpty shouldBe false
    config._2.size shouldBe 2
    config._2.getOrElse("key", "") shouldBe Set("value")
    config._2.getOrElse("key1", "") shouldBe Set("value1")


    val lineStreamWithoutDefault = Stream("[test]", "key=value", "key1<default>=value1")
    val configsWithoutDefault = configLoaderComp.configLoader.load(lineStreamWithoutDefault, None
      , new mutable.HashMap[String, Set[String]], new mutable.HashMap[String, mutable.Map[String, Set[String]]])

    configsWithoutDefault.isDefined shouldBe true
    val configWithoutDefault = configsWithoutDefault.get.head
    configWithoutDefault._1 shouldBe "test"
    configWithoutDefault._2.isEmpty shouldBe false
    configWithoutDefault._2.size shouldBe 1
    configWithoutDefault._2.getOrElse("key", "") shouldBe Set("value")

    val lineStreamWithDefault = Stream("[test]", "key=value", "key1<default>=value1")
    val configsWithDefault = configLoaderComp.configLoader.load(lineStream, None
      , new mutable.HashMap[String, Set[String]], new mutable.HashMap[String, mutable.Map[String, Set[String]]], Set("default"))

    configsWithDefault.isDefined shouldBe true
    val head = configsWithDefault.get.head
    head._1 shouldBe "test"
    head._2.isEmpty shouldBe false
    head._2.size shouldBe 2
    head._2.getOrElse("key", "") shouldBe Set("value")
    head._2.getOrElse("key1", "") shouldBe Set("value1")
  }


  "Load stream of invalid Strings " should " not create configs" in {
    val lineStream = Stream("key=value", "key1=value1")
    val configs = configLoaderComp.configLoader.load(lineStream, None
      , new mutable.HashMap[String, Set[String]], new mutable.HashMap[String, mutable.Map[String, Set[String]]])
    configs.isDefined shouldBe false
  }

  "Load stream of multiple String" should "create a complex configs" in {
    val lineStream = Stream("[test]", "key=value", "key1<default>=value1"
      , "[test1]", "key2 = value2"
      , "[test2]  ", "key3=value3;hello", "key<default>=defaultValue")

    val complexConfigs = configLoaderComp.configLoader.load(lineStream, None
      , new mutable.HashMap[String, Set[String]], new mutable.HashMap[String, mutable.Map[String, Set[String]]], Set("default"))

    complexConfigs.isDefined shouldBe true
    val complexConfigObjs = complexConfigs.get
    var configs = complexConfigObjs.get("test")
    configs.get("key") shouldBe Set("value")
    configs.get("key1") shouldBe Set("value1")

    configs = complexConfigObjs.get("test1")
    configs.get("key2") shouldBe Set("value2")

    configs = complexConfigObjs.get("test2")
    configs.get("key3") shouldBe Set("value3")
    configs.get("key") shouldBe Set("defaultValue")

  }
}
