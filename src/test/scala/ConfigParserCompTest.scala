import org.scalatest.{FlatSpec, Matchers}

class ConfigParserCompTest extends FlatSpec with Matchers {
  private val configParserComp = new ConfigRegexParserComp {}

  "valid group name " should "be extracted" in {
    val simpleName = configParserComp.configParser.parse("[helloWorld]")
    simpleName shouldBe Some(Group("helloWorld"))

    val nameWithAsterisk = configParserComp.configParser.parse("[hello*World]")
    nameWithAsterisk shouldBe Some(Group("hello*World"))

    val nameWithSpace = configParserComp.configParser.parse("[hello  world]")
    nameWithSpace shouldBe Some(Group("hello  world"))

    val nameWithPrefixAndSuffix = configParserComp.configParser.parse("[  helloWorld ]")
    nameWithPrefixAndSuffix shouldBe Some(Group("helloWorld"))
  }

  "invalid group name " should "return None" in {
    val emptyName = configParserComp.configParser.parse("[]")
    emptyName shouldBe None

    val spaceAsName = configParserComp.configParser.parse("[ ]")
    spaceAsName shouldBe None
  }

  "valid regular setting" should "be extracted" in {
    val simpleSetting = configParserComp.configParser.parse("key=value")
    simpleSetting shouldBe Some(Setting("key", Set("value")))

    val settingWithSpaces = configParserComp.configParser.parse("key = value  ")
    settingWithSpaces shouldBe Some(Setting("key", Set("value")))

    val settingWithComments = configParserComp.configParser.parse("key=value, value1;haha")
    settingWithComments shouldBe Some(Setting("key", Set("value", "value1")))
  }

  "invalid regular setting" should "return None" in {
    val emptySetting = configParserComp.configParser.parse(" =  ")
    emptySetting shouldBe None

    val settingWithEqualSymbol = configParserComp.configParser.parse("==value")
    settingWithEqualSymbol shouldBe None
  }

  "valid setting with default option" should "be extracted" in {
    val simpleSetting = configParserComp.configParser.parse("key<default>=value")
    simpleSetting shouldBe Some(DefaultSetting("key", "default", Set("value")))

    val simpleSettingWithSpace = configParserComp.configParser.parse("key <default> =value")
    simpleSettingWithSpace shouldBe Some(DefaultSetting("key", "default", Set("value")))
  }

}
