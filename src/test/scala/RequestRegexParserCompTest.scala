import org.scalatest.{FlatSpec, Matchers}

class RequestRegexParserCompTest extends FlatSpec with Matchers{
  val requestParser =  new RequestRegexParserComp {}.requestParser

  "simple node structure" should "be parsed into list of node names" in {
    val result = requestParser.parse("root.simpleNode")
    result.isEmpty shouldBe false
    result.head shouldBe "root"
    result.tail.head shouldBe "simpleNode"
  }

  "complex node structure" should "be parsed into list of node names" in {
    val result = requestParser.parse("root.complexNode[attributeNode]")
    result.isEmpty shouldBe false
    result shouldBe List("root","complexNode","attributeNode")
  }
}
